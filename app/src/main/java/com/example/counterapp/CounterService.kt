package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class CounterService : Service() {

    private var counter = 0

    inner class CounterBinder : Binder() {
        fun getService(): CounterService = this@CounterService
    }

    override fun onBind(intent: Intent?): IBinder {
        return CounterBinder()
    }

    fun presentValue() {
        Toast.makeText(this, "$counter", Toast.LENGTH_SHORT).show()
    }

    fun increment() {
        counter++
    }

    fun decrement() {
        counter--
    }
}
